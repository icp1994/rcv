from authlib.integrations.flask_client import OAuth
from flask import Flask

oauth = OAuth()


def create_app():
    app = Flask(__name__)

    app.config.from_object("config.Config")

    oauth.init_app(app)
    # From Lakin's code
    # https://github.com/lakinwecker/lichess-oauth-flask/blob/master/app.py
    oauth.register(
        "lichess",
        client_id=app.config["CLIENT_ID"],
        access_token_url="https://lichess.org/api/token",
        authorize_url="https://lichess.org/oauth",
        client_kwargs={"code_challenge_method": "S256"},
    )

    from app.auth import bp as auth_bp
    from app.rcv import bp as rcv_bp

    app.register_blueprint(auth_bp)
    app.register_blueprint(rcv_bp)

    return app
