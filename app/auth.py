import requests
from flask import Blueprint, current_app, redirect, render_template, session, url_for
from tinydb import TinyDB, where

bp = Blueprint("auth", __name__)

from app import oauth  # noqa: E402


@bp.route("/")
@bp.route("/auth")
def index():
    if session.get("voter_id") is not None:
        # Send authorized users directly to rcv page
        return redirect("/rcv")
    return render_template("auth/oauth.html")


@bp.route("/login")
def login():
    redirect_uri = url_for("auth.authorize", _external=True)
    return oauth.lichess.authorize_redirect(redirect_uri)


@bp.route("/authorize")
def authorize():
    try:
        token = oauth.lichess.authorize_access_token()
    except Exception:
        return render_template("auth/unauthorized.html")
    # Again from Lakin's code
    # https://github.com/lakinwecker/lichess-oauth-flask/blob/master/app.py
    bearer = token["access_token"]
    headers = {"Authorization": f"Bearer {bearer}"}
    resp = requests.get("https://lichess.org/api/account", headers=headers)
    lichess_id = resp.json()["id"]

    members = TinyDB(current_app.config["MEMBER_DB_PATH"])
    voter_slip = members.search(where("lichess_id") == lichess_id)
    if len(voter_slip) == 0:  # No associate for the id
        return render_template("auth/unauthorized.html")
    if len(voter_slip) > 1:  # Multiple associates for the id
        return render_template("auth/fraud.html", voter_slip=voter_slip)
    voter = voter_slip[0]

    # I feel terrible for doing this
    url = f"https://lichess.org/mod/{lichess_id}/permissions"
    cookies = {"lila2": current_app.config["LILA2_COOKIE"]}
    resp = requests.get(url, cookies=cookies)

    comms, boost, cheat = False, False, False
    if "Granted by package: SuperAdmin" in resp.text:
        comms, boost, cheat = True, True, True
    elif "Granted by package: Admin" in resp.text:
        comms, boost, cheat = True, True, True
    elif "Granted by package: Hunter" in resp.text:
        boost, cheat = True, True
    elif "Granted by package: Shusher" in resp.text:
        comms = True

    session["voter_id"] = voter.doc_id
    session["voter_name"] = lichess_id
    # Use the most constricted set of perms across lichess and zulip
    session["comms"] = comms & voter.get("comms", False)
    session["boost"] = boost & voter.get("boost", False)
    session["cheat"] = cheat & voter.get("cheat", False)

    return redirect("/rcv")


@bp.route("/logout")
def logout():
    session.clear()
    return redirect("/")
