from flask import Blueprint, redirect, render_template, session

bp = Blueprint("rcv", __name__, url_prefix="/rcv")


@bp.route("/")
def rcv():
    return redirect("/rcv/results")


@bp.route("/results")
def results():
    if not any(session.get(genre, False) for genre in ("comms", "boost", "cheat")):
        return render_template("auth/unauthorized.html")

    return render_template("rcv/rankings.html")
