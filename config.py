from os import environ, path

from dotenv import load_dotenv

load_dotenv()


class Config:
    CLIENT_ID = environ.get("CLIENT_ID")
    SECRET_KEY = environ.get("SECRET_KEY")
    TESTING = environ.get("TESTING").lower() == "true"
    LILA2_COOKIE = environ.get("LILA2_COOKIE")
    SESSION_COOKIE_SECURE = environ.get("SESSION_COOKIE_SECURE").lower() == "true"

    DB_ROOT_DIR = environ.get("DB_ROOT_DIR")
    MEMBER_DB_PATH = path.join(DB_ROOT_DIR, "member-db.json")
    PARTICIPANT_DB_PATH = path.join(DB_ROOT_DIR, "participant-db.json")
    TALLY_DB_PATH = path.join(DB_ROOT_DIR, "tally-db.json")
