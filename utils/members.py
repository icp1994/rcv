from os import environ, path

from dotenv import load_dotenv
from tinydb import TinyDB
from tinydb.operations import set as db_op_set
from tinydb.table import Document
from zulip import Client

load_dotenv()


def update_members(zulip_client, member_db):
    result = zulip_client.get_members({"include_custom_profile_fields": True})
    if result.get("result") == "success":
        members = result["members"]
    else:
        print("Error fetching organization members")
        print(result)
        return

    for member in members:
        if member["is_active"] and (not member["is_bot"]):
            record = {"zulip_full_name": member["full_name"]}
            # "6" is the identifier key corresponding to the custom
            # profile field named "Lichess" under  hq.lichess.ovh
            lichess_username = member["profile_data"].get("6")
            if lichess_username is not None:
                record["lichess_id"] = lichess_username["value"].lower()
            # If user unsets the "Liches" field after the first run,
            # that doesn't reflect in db.
            db.upsert(Document(record, doc_id=member["user_id"]))


def update_perms(zulip_client, member_db):
    result = zulip_client.get_subscriptions({"include_subscribers": True})
    if result.get("result") == "success":
        subscriptions = result["subscriptions"]
    else:
        print("Error fetching subscribed teams")
        print(result)
        return

    def subscriber_ids(stream):
        head, tail = len("user"), len("@hq.lichess.ovh")
        # "user32@hq.lichess.ovh" -> 32
        return [int(user[head:-tail]) for user in stream["subscribers"]]

    for stream in subscriptions:
        # If user leaves a stream after the first run, that doesn't reflect in db.
        if stream["name"] == "mod-comms-private":
            db.update(db_op_set("comms", True), doc_ids=subscriber_ids(stream))
        elif stream["name"] == "mod-hunter-boost":
            db.update(db_op_set("boost", True), doc_ids=subscriber_ids(stream))
        elif stream["name"] == "mod-hunter-cheat":
            db.update(db_op_set("cheat", True), doc_ids=subscriber_ids(stream))


if __name__ == "__main__":
    zuliprc_path = environ.get("ZULIPRC_PATH")
    db_root_dir = environ.get("DB_ROOT_DIR")
    zulip_client = Client(config_file=zuliprc_path)
    db = TinyDB(path.join(db_root_dir, "member-db.json"))

    update_members(zulip_client, db)
    update_perms(zulip_client, db)
