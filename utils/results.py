from heapq import heappop, heappush
from os import environ, path

from dotenv import load_dotenv
from tinydb import TinyDB

load_dotenv()


# Fun fact: this function works without requiring any arguments
def gnerate_leaderboards(tally, members, outdir):
    for genre in ("comms", "boost", "cheat"):
        standings, table_rows = [], []
        for mod in tally.table(genre):
            heappush(standings, (mod["score"], mod.doc_id))
        for idx in range(len(standings), 0, -1):
            mod = heappop(standings)
            table_rows.append(
                f"<tr><th scope='row'>{idx}</th>"
                f"<td>{mod[0]}</td>"
                f"<td>user{mod[1]}@hq.lichess.ovh</td>"
                f"<td>{members.get(doc_id=mod[1])['zulip_full_name']}</td></tr>"
            )
        with open(f"{outdir}/{genre}.html", "w") as outfile:
            outfile.writelines(table_rows[::-1])  # reversing the sort from min heap


if __name__ == "__main__":
    db_root_dir = environ.get("DB_ROOT_DIR")
    outdir = environ.get("LEADERBOARD_TEMPLATES_DIR")
    tally = TinyDB(path.join(db_root_dir, "tally-db.json"))
    members = TinyDB(path.join(db_root_dir, "member-db.json"))

    gnerate_leaderboards(tally, members, outdir)
